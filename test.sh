# Read inputs
for i in "$@"; do
    case $i in
        -l=*|--locations=*)
            LOCATIONS="${i#*=}" ;;
        *)
            ;;
    esac
    shift
done

LOCATIONS=($LOCATIONS)

echo "${LOCATIONS[@]}"

# Check that every location is a directory
for location in "${LOCATIONS[@]}"; do
    echo -e "\n location = $location"
done