#/usr/local/bin/bash

# backup_append --source=~/Pictures --destination=/Volumes/EXTERNAL_HDD

# Initialize defaults
NOSTATS="false"

# Read inputs
for i in "$@"; do
    case $i in
        -s=*|--source=*)
            SOURCE="${i#*=}" ;;
        -d=*|--destination=*)
            DESTINATION="${i#*=}" ;;
        --no-stats)
            NOSTATS="true" ;;
        *)
            ;;
    esac
    shift
done


# Check that source and destination are directories
if [ ! -d "$SOURCE" ]; then
    echo "Fatal: Source <$SOURCE> must be a directory."
    exit
fi

if [ ! -d "$DESTINATION" ]; then
    echo "Fatal: Destination <$DESTINATION> must be a directory."
    exit
fi

if [ "$NOSTATS" == "false" ]; then
    echo "$(tput setaf 2)Backing up $(tput setaf 3)$SOURCE ($(sudo du -chd 0 "$SOURCE" | head -n1 | awk '{ print $1 }') size) $(tput setaf 7)to $(tput setaf 4)$DESTINATION ($(sudo df -h "$DESTINATION" | tail -n1 | awk '{ print $4 }') free)$(tput setaf 7)..."
fi

sudo rsync\
    --super\
    --protect-args\
    --rsync-path="sudo rsync"\
    --archive\
    --update\
    --hard-links\
    --acls\
    --xattrs\
    --executability\
    --progress\
    --verbose\
    --verbose\
    --human-readable\
    --stats\
    "$SOURCE"\
    "$DESTINATION"

echo "$(tput setaf 1)Completed backup_append at $(date && tput setaf 7)"