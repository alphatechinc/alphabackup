#/usr/local/bin/bash
HOMEBREW_DIR="/usr/local/bin"
INSTALL_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

rm $HOMEBREW_DIR/backup_append
ln -s "$INSTALL_DIR/backup_append.sh" $HOMEBREW_DIR/backup_append

rm $HOMEBREW_DIR/backup_replicate
ln -s "$INSTALL_DIR/backup_replicate.sh" $HOMEBREW_DIR/backup_replicate

rm $HOMEBREW_DIR/backup_superset
ln -s "$INSTALL_DIR/backup_superset.sh" $HOMEBREW_DIR/backup_superset