#/usr/local/bin/bash

# backup_superset --locations="/Volumes/HDD1, /Volumes/HDD2, /Volumes/HDD3"
# (newest file across set of locations is identified and copied to all locations through n^2 ./backup_append operations; no files are deleted from any location, but the latest version across all locations becomes authoritative for each location)

# Initialize defaults
NOSTATS=""

# Read inputs
for i in "$@"; do
    case $i in
        -l=*|--locations=*)
            LOCATIONS="${i#*=}" ;;
        --no-stats)
            NOSTATS="--no-stats" ;;
        *)
            ;;
    esac
    shift
done

# Convert locations string into array
IFS=', ' read -a LOCATIONS <<< "$LOCATIONS"

# Check that every location is a directory
for location in "${LOCATIONS[@]}"; do
    if [ ! -d "$location" ]; then
        echo "Fatal: Location <$location> must be a directory."
        exit
    fi
done

# Sync locations, in each case using the most recent file
echo "$(tput setaf 1)Starting location sync at $(date && tput setaf 7)"
for location_from in "${LOCATIONS[@]}"; do
    echo "Syncing from $location_from..."
    for location_to in "${LOCATIONS[@]}"; do
        if [ "$location_to" != "$location_from" ]; then
                backup_append\
                $NOSTATS\
                --source="$location_from"\
                --destination="$location_to"
        fi
    done
done

echo "$(tput setaf 1)Completed backup_superset at $(date && tput setaf 7)"