# Errors installing on Catherine Cu's old laptop Nov 29
- Need to document symlinks for commands in the `my-backup*` scripts
- Alphabackup special case for git repos; could I just do a `alphabac_overwrite` command that doesn't have the `*git*` default exclude?
- Possibly, tighten up excludes on `Photos` mac app. Or, rather, only sync the `Masters` folder (so this shouldn't be a new feature here)

# To install

## Prepare the config files
- `bin/setup_variables.sh` # Common variables used in setup bash scripts
- `config/client_password.secret` # client password, stored in a file and read by the client; contains the raw password and nothing else
- `config/com.alphatec.backup.plist` # client backup plist; create from `.template`
- `config/rsyncd.conf.my_modules` # server modules that define names and paths of drives available to clients
- `config/rsyncd.secrets` # server secrets file; one line per `username:password` format
- create folder like `~/custom_backups`; it should contain the following symlinks:
    - `bin/alphabac_append` to `.`
    - `config/client_password.secret` to `.`
    - `bin/default_exclude` to `.`

## Execute install command
- `bin/install`

## Saving your install
- Good idea to `rsync -a bin/config ~/custom_backups`

## Debugging
- `config/org.samba.rsyncd.plist.template` # rsyncd template; this is the server template file that's filled out using `bin/install`
- `config/rsyncd.conf.template` # server config template base; `my_modules` is appended to this; includes a test module

# Tags
- 0.0.5 Major removal of old code; usedaemon is now the only option
- 0.0.4 Many tweaks to improve maintainability; running on new jbs-air
- 0.0.3 Working running on jbs-air (local only)
- 0.0.2 Working local daemon on john-imac
- 0.0.1 Working local daemon on susan-imac

# TODO low pri
- Backing up with links to preserve past snapshots: <https://blog.interlinked.org/tutorials/rsync_time_machine.html>
- Change how rsyncd.conf.template works; there should be a generator script that is separate from install, and install should expect an rsyncd.conf already
- Investigate rsync --link-dest for incremental snapshots
- See <http://www.howtogeek.com/175008/the-non-beginners-guide-to-syncing-data-with-rsync/>
- Consider making owner-drive read-only from remote; update via "pull" only
- Clean up args and use --name instead of positionals
- Consider encrypting the data at rest with a strong password stored in keychain
- For dupes, consider fdupes with link replacement "I just tried installing fdupes_1.50-PR2-4 on both Ubuntu and Debian, neither has the -L flag. Luckily building from github.com/tobiasschulz/fdupes was super easy. – neu242 Aug 30 '13 at 15:07"
- Also for dupes, see ``duff`` and ``hardlink`` and ``rdfind``
- For syncing, consider <https://github.com/sickill/bitpocket> (uses rsync)
- Change the plist name to something alphatec related, since this is software that uses rsyncd, not pure rsyncd

# Notes
- Tested with ``--checksum``; a series of local rsync with daemon took 1m30s without the option, and 54m with it.
- Homebrew doesn't come with ``munge-symlinks`` so I googled and downloaded it from here: <https://mattmccutchen.net/rsync/rsync.git/blob_plain/HEAD:/support/munge-symlinks>
- Daemon passwords are stored in secrets files on server and client

