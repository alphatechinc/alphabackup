SELECT '%marketing%' INTO @database;
SELECT "%channel%" INTO @columns;

SELECT
    phrases.table_schema,
    phrases.table_name,
    phrases.columns_count,
    phrases.columns_list,
    CONCAT(
        "SELECT \"%seach_inside_columns%\" INTO @content",
        "\n",";",
        "\nSELECT",
        "\n", phrases.select,",",
        "\n","  COUNT(1) AS `row_count`"
        "\n","FROM"
        "\n  ",phrases.table_schema,".",phrases.table_name,
        "\n","WHERE"
        "\n", phrases.where,
        "\n","GROUP BY",
        "\n",phrases.select,
        "\n","ORDER BY",
        "\n","  `row_count` DESC",
        "\n",";"
     ) AS `query`
FROM
(
    SELECT
        c.table_schema,
        c.table_name,
        CONCAT("  `",GROUP_CONCAT(c.column_name SEPARATOR "`,\n  `"),"`") AS `select`,
        CONCAT("  `",GROUP_CONCAT(c.column_name SEPARATOR "` LIKE @content\n  OR `"), "` LIKE @content") AS `where`,
        GROUP_CONCAT(DISTINCT c.column_name) AS columns_list,
        COUNT(DISTINCT c.column_name) AS columns_count
    FROM
        information_schema.columns AS c
    WHERE
        c.column_name LIKE @columns
        AND c.table_schema LIKE @database
    GROUP BY
        `table_name`
) AS phrases