#/usr/local/bin/bash

# Ask for password upfront
sudo -v
# Keep-alive: update existing `sudo` time stamp until this process has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Source folders list, separated by spaces. Omit trailing forward slash to copy the directory and contents vs. just directory contents.
src_folders=(
"$HOME/Dropbox/1Password"
)

for src in "${src_folders[@]}"; do
    backup_replicate --master="$src" --slave="/Volumes/PNY/"
    backup_replicate --master="$src" --slave="/Volumes/HKUST/"
    backup_replicate --master="$src" --slave="/Volumes/Samsung2GB/"
    backup_replicate --master="$src" --slave="/Volumes/Websense/"
done