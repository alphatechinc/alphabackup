#!/usr/local/bin/bash

function ex_rsync_options () {
    dests=(identical diffcontents difftimes)
    echo
    for d in "${dests[@]}"; do
        cmd="rsync --dry-run --itemize-changes -h $1 original/ $d"
        echo "$cmd"
        eval "$cmd"
    done
}

# -a = -rlptgoD =
# --recursive
# --links
# --perms
# --times (inverse of --ignore-times)
# --group
# --owner
# --devices
# --specials


##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# Untested
##### ##### ##### ##### ##### ##### ##### ##### ##### #####

# [none]; [bfile]; [none]
ex_rsync_options "--recursive --checksum --delete"

exit 0


##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# Tested; format: "# identical; diffcontents; difftimes"
##### ##### ##### ##### ##### ##### ##### ##### ##### #####

##### ##### ##### Winners ##### ##### #####

# [none]; [bfile]; [none]
ex_rsync_options "--recursive --checksum --delete"

##### ##### ##### Others ##### ##### #####

# [none]; [./, bfile]; [./, bfile]
ex_rsync_options "-a"

# [none]; [bfile]; [none]
ex_rsync_options "--delete --recursive --checksum"

# [none]; [bfile]; [bfile]
ex_rsync_options "--recursive"

# [none]; [./, bfile]; [./, bfile]
ex_rsync_options "--recursive --links --perms --times --group --owner --devices --specials"

# [none]; [./, bfile]; [./, bfile]
ex_rsync_options "--recursive --times"

# [none]; [bfile]; [none]
ex_rsync_options "--recursive --checksum"
