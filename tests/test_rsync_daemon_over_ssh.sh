script_root="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"


# Open the SSH tunnel
ssh\
    -F ssh_config\
    `#`\
    -f `# go into background`\
    -n `# prevent reading from stdin`\
    -N `# dont execute anything`\
    -T `# disable terminal`\
    test_ssh_host


# Get the port of the socket; unnecessary but the pid is potentially useful
ssh\
    `#`\
    -S test_rsyncd_socket `# name of control socket`\
    -O check `# run the check option`\
    test_ssh_host


# Execute the backup command
date > "$script_root/test_file_to_transfer"
cmd="../bin/alphabac_append\
    \"$script_root/test_file_to_transfer\"\
    \"alphabac@127.0.0.1::test_module\"\
    35150\
    \"../config/client_password.secret\"\
    "
echo "$cmd"
eval "$cmd"
rm -f "$script_root/test_file_to_transfer"


# Close the SSH tunnel
ssh\
    `#`\
    -S test_rsyncd_socket `# name of control socket`\
    -O exit `# exit the socket`\
    test_ssh_host