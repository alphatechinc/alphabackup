script_root="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

date > "$script_root/test_file_to_transfer"

cmd="../bin/alphabac_append\
    \"$script_root/test_file_to_transfer\"\
    \"alphabac@127.0.0.1::test_module\"\
    19925\
    \"../config/client_password.secret\"\
    "

echo "$cmd"
eval "$cmd"

rm -f "$script_root/test_file_to_transfer"