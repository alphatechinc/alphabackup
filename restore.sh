#/usr/local/bin/bash
#ln -s ~/gitrepos/alpha/backups/backup.sh /usr/local/bin/backup

# Ask for password upfront
sudo -v
# Keep-alive: update existing `sudo` time stamp until this process has finished
while true; do sudo -n true && sleep 60 && kill -0 "$$" || exit; done 2>/dev/null &

export src_base="/Volumes/ROAM_IDRIVE/Users"

declare -A restores
restores["$src_base/Music/"]="$(echo ~)/Music/"
restores["$src_base/Pictures/"]="$(echo ~)/Pictures/"
restores["$src_base/jbryanscott/"]="$(echo ~)/gitrepos/from_backup_drive/"

echo "$(tput setaf 1)Starting restore at $(date && tput setaf 7)"
for src in "${!restores[@]}"; do
    dest="${restores["$src"]}"
    echo "Restoring from $src to $dest..."
    if [ -d "$dest" ]
        then
            echo "$(df -h "$dest" | awk '{print $4}' | tail -n 1) free on $dest"
            eval $(echo sudo rsync -avhHAXEx --progress --stats "\"$src\"" "\"$dest\"")
        else
            echo "$(tput setaf 1)Fatal: $dest does not exist. Skipping.$(tput setaf 7)"
    fi
    echo
done
echo "$(tput setaf 1)Completed restore at $(date && tput setaf 7)"
