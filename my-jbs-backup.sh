#/usr/local/bin/bash

# Ask for password upfront
sudo -v
# Keep-alive: update existing `sudo` time stamp until this process has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Source folders list, separated by spaces. Omit trailing forward slash to copy the directory and contents vs. just directory contents.
src_folders=(
"$HOME/Dropbox"
"$HOME/gitrepos"
"$HOME/Movies"
"$HOME/Music"
"$HOME/Pictures"
"$HOME/Library/Application Support/MobileSync"
"$HOME/Evernote"
"$HOME/Library/Containers/com.wunderkinder.wunderlistdesktop"
"$HOME/VPS-Backups"
"$HOME/Google-Drive-Export"
)

for src in "${src_folders[@]}"; do
    backup_append --source="$src" --destination="/Volumes/ROAM_SP_BLUE/Users/"
done

backup_superset --locations="/Volumes/ROAM_SP_BLUE/, /Volumes/ROAM_SP_GREEN/"