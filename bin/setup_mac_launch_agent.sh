plist_name="$__my_backup_name"
source_path="$script_root/../config/$plist_name.plist.template"
install_path="$__my_backup_root/$plist_name.plist"
my_backup="$__my_backup_script"
my_backup_dir=`dirname "$(greadlink -f $my_backup)"`

if [ ! -f "$source_path" ]; then echo "Source path doesn't exist; you may need to create it from the template; aborting: $source_path"; exit 1; fi
if [ -z "$plist_name" ]; then echo "Plist name is empty."; exit 1; fi
if [ ! -f "$my_backup" ]; then echo "My backup path doesn't exist; aborting: $my_backup"; exit 1; fi

launchctl unload "$install_path" > /dev/null 2>&1

install_and_backup "$source_path" "$install_path"
chgrp staff "$install_path"

launchctl load   "$install_path"

