# Common variables used by the setup files

__log_root="/var/log/alphabac"
__etc_root="/etc/rsyncd"

__conf_name="rsyncd.conf"
__conf_path="$__etc_root/$__conf_name"

__secrets_name="rsyncd.secrets"
__secrets_path="$__etc_root/$__secrets_name"

__daemon_root="/Library/LaunchDaemons"
__daemon_name="org.samba.rsyncd"
__port=19925

# be sure to use the dot-dir before the final directory; it's a security feature defined in man rsyncd.conf
__test_path="$HOME/test_rsync_daemon/./test1"

__my_backup_root="$(greadlink -f ~/Library/LaunchAgents)"
__my_backup_name="com.alphatec.backup.agent"
__my_backup_script="$(greadlink -f ~/my-backup/backup)"

