source_root="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
install_root="$__daemon_root"
plist_name="$__daemon_name"

source_path="$source_root/../config/$plist_name.plist.template"
install_path="$install_root/$plist_name.plist"

if [ ! -f "$source_path" ]; then
    echo "Source path doesn't exist; aborting: $source_path"
    exit 1
fi

if [ -z "$install_path" ]; then echo "Install path is empty."; exit 1; fi
if [ -z "$plist_name" ]; then echo "Plist name is empty."; exit 1; fi

install_and_backup "$source_path" "$install_path"

# Permissions
sudo chown root:wheel "$install_path"
sudo chmod 644        "$install_path"

# Reinstall
sudo launchctl unload "$install_root/$plist_name.plist"
sudo launchctl load   "$install_root/$plist_name.plist"

