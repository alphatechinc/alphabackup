# This file sets up the `rsync` client logs for writing by `alphabac_append` and others.

log_root="$__log_root"
sudo mkdir -p "$log_root"

if [ ! -d "$log_root" ]; then
    echo "Unable to create log directory: $log_root"
    exit 1
fi

sudo chmod 777 "$log_root"

