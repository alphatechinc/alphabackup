# Common functions used by the setup files

function install_and_backup() {
    if [ -z "$__port" ]\
        || [ -z "$__secrets_path" ]\
        || [ -z "$__conf_path" ]\
        || [ -z "$__test_path" ]\
        ; then
        echo "Fatal: template variable not defined. See install."
        exit 1
    fi

    source_path="$1"
    install_path="$2"
    if [ -f "$install_path" ]; then
        sudo cp -a "$install_path" "$install_path-$(date "+%F-%T")-$(uuidgen).bak"
    fi

    sudo mkdir -p "$(dirname $install_path)"

    # Compile template variables

    cmd="sudo cat $source_path\
        | sed -E\
            -e \"s/\{\{port\}\}/$(echo $__port | sed -e 's/\//\\\//g' -e 's/\./\\\./g')/g\"\
            -e \"s/\{\{secrets_path\}\}/$(echo $__secrets_path | sed -e 's/\//\\\//g' -e 's/\./\\\./g')/g\"\
            -e \"s/\{\{conf_path\}\}/$(echo $__conf_path | sed -e 's/\//\\\//g' -e 's/\./\\\./g')/g\"\
            -e \"s/\{\{test_path\}\}/$(echo $__test_path | sed -e 's/\//\\\//g' -e 's/\./\\\./g')/g\"\
            -e \"s/\{\{my_backup\}\}/$(echo $my_backup | sed -e 's/\//\\\//g' -e 's/\./\\\./g')/g\"\
            -e \"s/\{\{my_backup_dir\}\}/$(echo $my_backup_dir | sed -e 's/\//\\\//g' -e 's/\./\\\./g')/g\"\
         "

    uuid="$(uuidgen)"
    eval "$cmd > /tmp/$uuid"
    sudo mv "/tmp/$uuid" "$install_path"
}

