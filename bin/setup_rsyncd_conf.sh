# This file compiles and sets up the `rsyncd.conf` file.
# The config file governs the behavior of the `rsyncd` server.

source_root="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
install_root="$__etc_root"
file_name="$__conf_name"
file_name="rsyncd.conf"

source_path="$source_root/../config/$file_name.template"
install_path="$install_root/$file_name"

if [ ! -f "$source_path" ]; then
    echo "Source path doesn't exist; aborting: $source_path"
    exit 1
fi

if [ -z "$install_path" ]; then echo "Install path is empty."; exit 1; fi

install_and_backup "$source_path" "$install_path"
cat "$script_root/../config/rsyncd.conf.my_modules" >> "$install_path"

# Permissions
sudo chown root:wheel "$install_path"
sudo chmod 600        "$install_path"

