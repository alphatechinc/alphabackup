# Sets up the test path for rsyncd, which defaults to `$HOME/test_rsync_daemon/./test1"

test_path="$__test_path"
mkdir -p "$test_path"

if [ ! -d "$test_path" ]; then
    echo "Test destination isn't a directory! Aborting: $test_path"
    exit 1
fi

chmod 767 "$test_path"

