# Installs `rsyncd.secrets` to `/etc/rsyncd`
# This file contains the `rsyncd` server's username and password.

source_root="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
install_root="$__etc_root"
file_name="$__secrets_name"

source_path="$source_root/../config/$file_name"
install_path="$install_root/$file_name"

if [ ! -f "$source_path" ]; then
    echo "Source path doesn't exist; aborting: $source_path"
    exit 1
fi

if [ -z "$install_path" ]; then echo "Install path is empty."; exit 1; fi

install_and_backup "$source_path" "$install_path"

# Permissions
sudo chown root:wheel "$install_path"
sudo chmod 600        "$install_path"

