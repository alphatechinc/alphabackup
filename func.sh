#!/usr/local/bin/bash
# usage ./func.sh -a="hello world" -e="asdf" -s="bob" -l="joe"

for i in "$@"; do
    case $i in
        -e=*|--extension=*)
            EXTENSION="${i#*=}" ;;
        -s=*|--searchpath=*)
            SEARCHPATH="${i#*=}" ;;
        -l=*|--lib=*)
            LIBPATH="${i#*=}" ;;
        --default)
            DEFAULT=YES ;;
        *)
            ;;
    esac
    shift
done
echo "FILE EXTENSION  = ${EXTENSION}"
echo "SEARCH PATH     = ${SEARCHPATH}"
echo "LIBRARY PATH    = ${LIBPATH}"
echo "Number files in SEARCH PATH with EXTENSION:" $(ls -1 "${SEARCHPATH}"/*."${EXTENSION}" | wc -l)
if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 $1
fi