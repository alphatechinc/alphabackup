#/usr/local/bin/bash
#ln -s ~/gitrepos/jbryanscott/backups/backup.sh /usr/local/bin/backup

# Ask for password upfront
sudo -v

src_folders=( ~/Dropbox/1Password ) 
# Names of random USB drives I've accumulated
dest_folder_parents=( /Volumes/PNY /Volumes/HKUST /Volumes/Samsung2GB /Volumes/Websense )

echo "$(tput setaf 1)Starting backup at $(date && tput setaf 7)"
for dest in "${dest_folder_parents[@]}"; do
    echo "Backing up to $dest..."
    df -h $dest
    for src in "${src_folders[@]}"; do
        echo "$(tput setaf 2)Backup up $(tput setaf 3)$src $(tput setaf 7)to $(tput setaf 4)$dest$(tput setaf 7)..."
        du -m -d 0 "$src"
        test -e "$dest" && sudo rsync -avhHAXEx --protect-args --progress --stats "$src" "$dest"
    done
done
echo "$(tput setaf 1)Completed backup at $(date && tput setaf 7)"
