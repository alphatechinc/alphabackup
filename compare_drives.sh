rsync \
    --dry-run \
    -avh \
    --progress \
    --exclude '.com.apple.timemachine.donotpresent' \
    --exclude '.Spotlight-V100' \
    --exclude '.Trashes' \
    --exclude '.fseventsd' \
    --exclude '.DS_Store' \
    "/Volumes/ROAM_VENUS/" \
    "/Volumes/ROAM_IDRIVE/"