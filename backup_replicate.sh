#/usr/local/bin/bash

# backup_replicate --master=/Volumes/Master --slave=/Volumes/Slave
# (master is strictly copied to slave and non-referenced files on the slave are deleted)

# Initialize defaults
NOSTATS="false"

# Read inputs
for i in "$@"; do
    case $i in
        --master=*)
            MASTER="${i#*=}" ;;
        --slave=*)
            SLAVE="${i#*=}" ;;
        --no-stats)
            NOSTATS="true" ;;
        *)
            ;;
    esac
    shift
done


# Check that MASTER and SLAVE are directories
if [ ! -d "$MASTER" ]; then
    echo "Fatal: Master <$MASTER> must be a directory."
    exit
fi

if [ ! -d "$SLAVE" ]; then
    echo "Fatal: Slave <$SLAVE> must be a directory."
    exit
fi

if [ "$NOSTATS" == "false" ]; then
    echo "$(tput setaf 2)Replicating $(tput setaf 3)master $MASTER ($(sudo du -chd 0 "$MASTER" | head -n1 | awk '{ print $1 }') size) $(tput setaf 7)to $(tput setaf 4)slave $SLAVE ($(sudo df -h "$SLAVE" | tail -n1 | awk '{ print $4 }') free)$(tput setaf 7)..."
fi

sudo rsync\
    --super\
    --protect-args\
    --rsync-path="sudo rsync"\
    --archive\
    --delete-after\
    --hard-links\
    --acls\
    --xattrs\
    --executability\
    --progress\
    --verbose\
    --verbose\
    --human-readable\
    --stats\
    "$MASTER"\
    "$SLAVE"

echo "$(tput setaf 1)Completed backup_replicate at $(date && tput setaf 7)"